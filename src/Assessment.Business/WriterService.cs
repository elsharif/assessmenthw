﻿using Assessment.Interfaces;

namespace Assessment.Business
{
    /// <summary>
    /// Service to write message to different mediums
    /// </summary>
    public class WriterService
    {
        private readonly IWriter _writer;

        public WriterService(IWriter writer)
        {
            _writer = writer;
        }

        public void WriteMessage(string message)
        {
            _writer.Write(message);
        }
    }
}