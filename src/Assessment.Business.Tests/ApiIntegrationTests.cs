﻿using Assessment.Interfaces;
using Assessment.Repositories;
using Microsoft.Practices.Unity;
using NUnit.Framework;

namespace Assessment.Business.Tests
{
    [TestFixture]
    public class ApiIntegrationTests
    {
        private IUnityContainer _container;


        [TestFixtureSetUp]
        public void TestFixtureSetup()
        {
            _container = new UnityContainer();
        }


        [Test]
        public void Writer_WriteToConsole_MessagePrinted()
        {
            _container.RegisterType<IWriter, ConsoleWriter>();

            _container.RegisterType<WriterService, WriterService>();

           var writerService = _container.Resolve(typeof (WriterService)) as WriterService;
            
            writerService.WriteMessage("HelloWorld");
            
        }
    }
}
