﻿using System;
using Assessment.Interfaces;
using Moq;
using NUnit.Framework;

namespace Assessment.Business.Tests
{
    [TestFixture]
    public class ApiTests
    {
        private readonly Mock<IWriter> _writerMock = new Mock<IWriter>();

        [Test]
        public void Write_ToConsole_WrittenToConsole()
        {
            var message = "Hello World";
            _writerMock.Setup(p => p.Write(It.IsAny<string>())).Callback<string>(Console.WriteLine);
            var writerServiceMock = new Mock<WriterService>(_writerMock.Object);

            writerServiceMock.Object.WriteMessage(message);

            _writerMock.Verify(x => x.Write(message), Times.Once);
        }
    }
}