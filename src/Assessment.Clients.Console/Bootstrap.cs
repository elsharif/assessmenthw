﻿using System;
using System.Configuration;
using Assessment.Business;
using Assessment.Interfaces;
using Assessment.Repositories;
using Microsoft.Practices.Unity;

namespace Assessment.Clients.Console
{
    /// <summary>
    /// This is where you configure which writers to use
    /// </summary>
    public static class Bootstrap
    {
        /// <summary>
        /// Configure writers 
        /// </summary>
        /// <param name="container"></param>
        public static WriterService ConfigureDependencies(IUnityContainer container)
        {
            if (ConfigurationManager.AppSettings.Get("Writer").Equals("db",StringComparison.InvariantCultureIgnoreCase))
            {
                container.RegisterType<IWriter, DatabaseWriter>();
            }
            else
            {
                container.RegisterType<IWriter, ConsoleWriter>();
            }
            
            container.RegisterType<WriterService, WriterService>();

            return ResolveDependencies(container);
        }
        /// <summary>
        /// Resolve the dependency tree starting at the root service
        /// </summary>
        /// <param name="container"></param>
        private static WriterService ResolveDependencies(IUnityContainer container)
        {
           return container.Resolve<WriterService>();
        }
    }
}