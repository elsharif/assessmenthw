﻿using System;
using Microsoft.Practices.Unity;

namespace Assessment.Clients.Console
{
    class Program
    {
        static void Main()
        {
            var service = Bootstrap.ConfigureDependencies(new UnityContainer());
            service.WriteMessage("Hello World!");
        }
    }
}
