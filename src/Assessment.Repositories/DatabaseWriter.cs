﻿using System;
using Assessment.Interfaces;

namespace Assessment.Repositories
{
    public class DatabaseWriter: IWriter
    {
        //logic to write to database 
        public void Write(string message)
        {
            //This is just to how another type of writer is supported. Should have logic to persist to db
            Console.WriteLine("Witing to db...");
            Console.WriteLine(message);
            Console.Read();
        }
    }
}