﻿using System;
using Assessment.Interfaces;

namespace Assessment.Repositories
{
    public class ConsoleWriter : IWriter
    {
        public void Write(string message)
        {
            Console.WriteLine("Writing to Console...");
            Console.WriteLine(message);
            Console.Read();
        }
    }
}