﻿namespace Assessment.Interfaces
{
    public interface IWriter
    {
        void Write(string message);
    }
}