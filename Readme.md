![Architecture](https://bytebucket.org/elsharif/assessmenthw/raw/a5ef00db993495be61089f866e2b2db45b8e1a94/architecture.png)
## Introduction
Simple App to support different "Hello World" Writers. The app is built with .Net Framework 4.0. It's architected in 
three layers:

## 1. Repository layer
	This is a data layer that uses two different repositories to write to a database or a console. 
	The repositories implemments a common interface IWriter that abstracts implementation
##   2. Interface Layer
	This a layer for contracts/interfaces that's will be used to abstract any implemenation and will be used by 
	the host/client to define the implemenation of writers
 
##  3. Business/Service Layer
	This is a service/Facade` layer that uses the writer interface to write to any destination. Client will use
	this service to to call writers by injecting whichever writerthey want to write to using IOC.
## 4. Console/client
	This is ia console app that uses the Business layer to write a hello world to either a db or console writer. It uses Unity IOC to inject 
	the implementation of the writer into the service/Business layer based on a configuration. In the app.config file you can set 
	the writer appsetting to "DB" to write to a db or "Console" to write to a console.
	The depdendencies and IOC configuration is done in the bootstrap file. This is the only place where dependencies are defined in the app.

## Unit Tests
	You will find a test project under the test folder. The tests uses Moq for mocking dependencies and NUnit to run the tests.

## Build and Run
	Included with the project is an MSBuild file and a runner to execute the build and run the tests. The build file will 
	restore nuget packages, build the solution and execute unit tests. It also runs a coverage report 
	.The test results and coverage report will be created under an artifact folder.
	If you get MSBuild errors edit the location of MSBuild in the runner to match your system. The assumptions made are 
	that you have %windir% variable in your system pointing to Windows folder and that you have 64 bit systems

	There is also  nuget.exe file in the Tools folder  that will be used to restore and download testing/report packages.
 
	
